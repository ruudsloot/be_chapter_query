package com.ruud.demo;

import static com.ruud.demo.Generator.randomFarmer;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	@PersistenceContext
	EntityManager entityManager;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(FarmerRepository repository) {
		return (args) -> {
			// save a few farmers
			IntStream.range(0, 5)
					.forEach(i -> repository.save(randomFarmer()));

			log.info("---jpaQuery START---");
			jpaQuery().forEach(farmer -> {
				log.info("--------------------------------");
				log.info("Farmer: {} has {} cows", farmer.getName(), farmer.getCows().size());
				log.info("");
			});
			log.info("---jpaQuery STOP---");

			log.info("---criteriaBuilder START---");
			criteriaBuilder().forEach(farmer -> {
				log.info("--------------------------------");
				log.info("Farmer: {} has {} cows", farmer.getName(), farmer.getCows().size());
				log.info("");
			});
			log.info("---criteriaBuilder STOP---");

		};
	}

	private List<Farmer> jpaQuery() {
		// TODO
		return Collections.emptyList();
	}

	private List<Farmer> criteriaBuilder() {
		// TODO
		return Collections.emptyList();
	}

}
