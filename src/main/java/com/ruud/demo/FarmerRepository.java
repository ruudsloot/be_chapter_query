package com.ruud.demo;

import org.springframework.data.repository.CrudRepository;

public interface FarmerRepository extends CrudRepository<Farmer, Long> {

}
