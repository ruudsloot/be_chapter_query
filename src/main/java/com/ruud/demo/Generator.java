package com.ruud.demo;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;

public class Generator {
    private static final Faker faker = new Faker();

    public static List<Cow> randomCows() {
        return IntStream.range(1, randomInt(5, 20))
                .mapToObj(i -> new Cow(faker.name().name()))
                .collect(Collectors.toList());
    }

    public static Farmer randomFarmer() {
        final Name name = faker.name();
        return new Farmer(name.firstName(), name.lastName(), randomCows());
    }

    private static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
