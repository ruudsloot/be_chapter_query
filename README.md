# Getting Started

start database
```shell
./run_database.sh
```

start application
```shell
./gradlew bootRun
```
